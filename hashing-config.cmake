#
# Library configuration file used by dependent projects.
# @file:   hashing-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED hashing_FOUND)

  # Locate the library headers.
  find_path(hashing_include_dir
    NAMES hashing.h
    PATHS ${hashing_DIR}/include
  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    hashing DEFAULT_MSG
    hashing_include_dir
  )
endif()
