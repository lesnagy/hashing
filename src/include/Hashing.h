#ifndef HASHING_H_
#define HASHING_H_

namespace hashing {

template <typename T>
inline void hash_combine(size_t & seed, const T& v) 
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

} // namespace hashing


#endif  // HASHING_H_
